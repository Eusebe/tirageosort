# tirageOsort

associates randomly by pairs elements from a list, excluding association of elements from the same group

## About

A JavaScript library by Eusebe.

See the [project homepage](https://bitbucket.org/Eusebe/tirageosort/).

## Installation

Using Bower:

    bower install tirageOSort

Or grab the [source](https://bitbucket.org/Eusebe/tirageosort/dist/tirageAuSort.js) ([minified](https://bitbucket.org/Eusebe/tirageosort/dist/tirageAuSort.min.js)).

## Usage

Basic usage is as follows:

    persons = [ {name:'mandatory', group:'optionnaly'}, ...];
    generateDraw(persons);

For advanced usage, see the documentation.

## Documentation

Start with `docs/MAIN.md`.

## Contributing

We'll check out your contribution if you:

* Provide a comprehensive suite of tests for your fork.
* Have a clear and documented rationale for your changes.
* Package these up in a pull request.

We'll do our best to help you out with any contribution issues you may have.

## License

MIT. See `LICENSE.txt` in this directory.