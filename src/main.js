/* tirageAuSort main */



var participants = [	{name:'Anicette', group:1},
						{name:'Jean-Noel', group:1},
						{name:'Olivier', group:2},
						{name:'Elisa', group:2},
						{name:'Solal', group:2},
						{name:'Eléa', group:2},
						{name:'Pierre', group:3},
						{name:'Hélène', group:3},
						{name:'Anouk', group:3},
						{name:'Maxence', group:3},
						{name:'Jean', group:4},
						{name:'Dorothée', group:4},
						{name:'Matthieu', group:5},
						{name:'Aurélie', group:5}
					];


var generateDraw = function(persons, counter) {
	if (counter > 10) {
		throw 'no solution found.';
	}
	if (isUndefinedOrNull(persons) || isMalformed(persons)) {
		return [];
	}
	var result = [];
	var toAttribute = persons.slice(0);
	for (var i = persons.length - 1; i >= 0; i--) {
		var found = false;
		while (!found){
			// if all toAttribute people are from the group of the current person, restarting the procedure
			if (!areAllElementsFromDifferentGroup(toAttribute, persons[i].group)){
				if (isUndefinedOrNull(counter)){
					counter = 0;
				}
				counter++;				
				console.log('## re-generating draw: x'+counter);
				return generateDraw(persons, counter);
			}
			var index = Math.floor(Math.random()*toAttribute.length);
			if ((isUndefinedOrNull(persons[i].group) || persons[i].group != toAttribute[index].group) && persons[i].name != toAttribute[index].name){
				// console.log(persons[i].name + ' => ' + toAttribute[index].name);
				result.push({gives:persons[i], receives:toAttribute[index]});
				toAttribute.splice(index, 1);
				found = true;
			}
		}
	}
	return result;
};

function isUndefinedOrNull(val){
	return val === undefined || val === null;
}

function isMalformed(elements) {
	if (isUndefinedOrNull(elements) || !Array.isArray(elements) || elements.length === 0){
		return true;
	}
	var malformed = false;
	for (var i = elements.length - 1; i >= 0; i--) {
		if (isUndefinedOrNull(elements[i].name)){
			// console.log('element #' + i + " from input is malformed");
			malformed = true;
		}
	}
	return malformed;
}

var areAllElementsFromDifferentGroup = function(list, group){
	if (isUndefinedOrNull(list) || !Array.isArray(list) || list.length === 0 || isNaN(group) || isUndefinedOrNull(group)){
		return true;
	}
	for (var i = list.length - 1; i >= 0; i--) {
		if (list[i].group != group){
			return true;
		}
	}
	console.log('## all leaving elements are from the group ' + group);
	return false;
};

var displayResult = function(result) {
	for (var i = result.length - 1; i >= 0; i--) {
		console.log(result[i].gives.name + ' gives to ' + result[i].receives.name);
	}
};

// Export to the root, which is probably `window`.
root.generateDraw = generateDraw;
root.displayResult = displayResult;
root.participants = participants;
root.isMalformed = isMalformed;
root.areAllElementsFromDifferentGroup = areAllElementsFromDifferentGroup;

// Version.
generateDraw.VERSION = '0.1.0';


