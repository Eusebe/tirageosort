test("the base function exists", function() {
	ok(generateDraw, "function exists");
});

test("generateDraw",function(){
	var result = generateDraw(participants);
	ok(result, "result exists and is defined");
	ok(Array.isArray(result), "result is an array");

	var givers = [];
	var receivers = [];
	for (var i = result.length - 1; i >= 0; i--) {
		ok(result[i].gives, "result#" + i + " has an attribute called 'gives'");
		equal(givers.indexOf(result[i].gives), -1, "giver#" + i + "(" + result[i].gives.name + ") does not already exist.");
		givers.push(result[i].gives);
		ok(result[i].receives, "result#" + i + " has an attribute called 'gives'");
		equal(receivers.indexOf(result[i].receives), -1, "receiver#" + i + "(" + result[i].receives.name + ") does not already exist.");
		receivers.push(result[i].receives);
	};
});

test("isMalformed checks that ", function(){
	ok(isMalformed(), 'an empty parameter is not valid');
	ok(isMalformed({name:'bidule'}), 'an object which is not an array is not valid');
	ok(isMalformed([]), 'an empty array returns is not valid');
	ok(isMalformed([{na:'me'}]), 'an element with no "name" attribute is not valid');
	ok(!isMalformed([{name:'me'}]), 'an element with a "name" attribute is valid');
	ok(!isMalformed([{name:'me', truc:''}, {name:'machin'}, {name:''}]), 'an element with a "name" attribute is valid');
});

test("generateDraw returns ", function(){
	var bidule;
	var result = generateDraw(bidule);
	equal(result.length, 0, 'an empty array if the input parameter is invalid (empty / undefined)');
	equal(generateDraw().length, 0, 'an empty array if the input parameter is invalid (empty / undefined)');
	//deal with malformed inputs
	equal(generateDraw([{name:'bidule'}, {noNameAttribute:''}]).length, 0, 'an empty array if the input is malformed');
	// valid input with group
	equal(generateDraw([{name:'Niobé', group:1}, {name:'Julia', group:2}]).length, 2, 'a well-formed array if input values have no group');
	// valid input without group
	result = generateDraw(incompleteGroupData);
	equal(result.length, 2, 'a well-formed array if input values have no group');
	equal(result[0].gives, result[1].receives, 'cross-association giver-receiver');
	equal(result[1].gives, result[0].receives, 'cross-association giver-receiver');

	equal(generateDraw(dataWithNullGroup).length, 2, 'a well-formed array if input values have null group');
});

test("areAllElementsFromDifferentGroup checks that ", function(){
	ok(areAllElementsFromDifferentGroup([], 1), 'empty array is considered from different group');
	ok(areAllElementsFromDifferentGroup(incompleteGroupData, 2), 'empty group attribute is considered as different');
	ok(areAllElementsFromDifferentGroup([{name:'bidule', group:1}, {name:'', group:2}], 1), 'at least one element out of the given group returns true');
	ok(!areAllElementsFromDifferentGroup([{name:'bidule', group:1}], 1), 'element in given group returns false');
	ok(areAllElementsFromDifferentGroup([{name:'bidule'}]), 'no group returns true (even if no group is specified in elements)');
});

var incompleteGroupData = [{name:'Niobé'}, {name:'Julia'}];
var dataWithNullGroup = [{name:'Niobé', group:null}, {name:'Julia', group:null}];